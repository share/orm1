<?php

namespace ORM;

/**
 * Class TemplateRepository
 * @package ORM
 */
class TemplateRepository extends Repository
{
	/**
	 * TemplateRepository constructor.
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->entityName = '';

		parent::__construct();
	}

	/**
	 * @return _Entity|_Entity[]
	 * @throws BadRequestException
	 * @throws Exception
	 */
	public function populateEntity()
	{
		return parent::populateEntity();
	}

	/**
	 * @return _Entity[]
	 * @throws Exception
	 */
	public function getAll()
	{
		return parent::getAll();
	}

	/**
	 * @return _Entity[]
	 * @throws Exception
	 */
	public function getAllBy()
	{
		return parent::getAllBy();
	}

	/**
	 * @return _Entity|null
	 * @throws Exception
	 */
	public function getOne()
	{
		return parent::getOne();
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function doesEntityExist()
	{
		return parent::doesEntityExist();
	}

	/**
	 * @param mixed ...$args
	 *
	 * @return \Dibi\Fluent
	 * @throws Exception
	 */
	public function makeSelect(...$args)
	{
		return parent::makeSelect(...$args);
	}

	/**
	 * @param string $table
	 * @param iterable $args
	 *
	 * @return \Dibi\Fluent
	 * @throws Exception
	 */
	public function makeUpdate(string $table, iterable $args)
	{
		return parent::makeUpdate($table, $args);
	}

	/**
	 * @param string $table
	 * @param iterable $args
	 *
	 * @return \Dibi\Fluent
	 * @throws Exception
	 */
	public function makeInsert(string $table, iterable $args)
	{
		return parent::makeInsert($table, $args);
	}

	/**
	 * @param string $table
	 *
	 * @return \Dibi\Fluent
	 * @throws Exception
	 */
	public function makeDelete(string $table)
	{
		return parent::makeDelete($table);
	}

	/**
	 * @return bool
	 * @throws BadRequestException
	 * @throws Exception
	 */
	public function save()
	{
		return parent::save();
	}
}