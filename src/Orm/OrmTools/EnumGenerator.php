<?php

namespace ORM;

/**
 * Class EnumGenerator
 * @package ORM
 */
final class EnumGenerator
{
	public function __construct()
	{
	}

	/**
	 * @param string $dbName
	 * @param string $tableName
	 * @param string $enumName
	 * @param string $keyColumn
	 * @param string $valueColumn
	 */
	public function generate($dbName, $tableName, $enumName, $keyColumn, $valueColumn = null)
	{
		$dibi = Manager::getInstance()->getDBConnection();

		$data = $dibi->select(sprintf('%s AS _key, %s AS _value', $keyColumn, empty($valueColumn) ? $keyColumn : $valueColumn))
			->from(sprintf('%s.%s', $dbName, $tableName))
			->fetchPairs('_key', '_value');

		if (!$data) {
			return;
		}

		$className = $enumName . 'Enum';

		$fileContent = '<?php

namespace ORM;' . "\n\n";

		$fileContent .= 'class ' . $className . ' extends Enum' . "\n";
		$fileContent .= '{';

		foreach ($data as $key => $value) {
			$label = $key;
			$keyName = empty($valueColumn) ? 'E' . $key : $key;
			$keyName = Utils::removeAccents($keyName);
			$keyName = str_replace([' ', ',', '.', '-', '/', '\\'], ['_', '', '', '_', '_', '_'], $keyName);
			$keyName = strtoupper($keyName);

			$fileContent .= '
	/**
	 * @label ' . $label . '
	 */
	CONST ' . $keyName . ' = ' . $value . ';';
		}

		$fileContent .= "\n" . '}';

		$enumFilePath = Manager::getInstance()->getConfig()->enumsFolder;
		$enumFileName = $enumFilePath . '/' . $className . '.php';

		if (!file_exists($enumFilePath)) {
			mkdir($enumFilePath, 0777, true);
		}

		file_put_contents($enumFileName, $fileContent);
	}
}