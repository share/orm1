<?php

namespace ORM;

/**
 * Class EntityGenerator
 * @package ORM
 */
final class EntityGenerator
{
	public function __construct()
	{
	}

	/**
	 * @param string $dbName
	 * @param string $tableName
	 * @param string $entityName
	 */
	public function generate($dbName, $tableName, $entityName)
	{
		$dibi = Manager::getInstance()->getDBConnection();

		$columns = $dibi->select('c.COLUMN_NAME, c.DATA_TYPE, c.COLUMN_KEY')
			->from('information_schema.tables')->as('t')
			->from('information_schema.columns')->as('c')
			->where('t.table_name = c.table_name AND t.table_schema = c.table_schema AND t.table_name = %s AND t.table_schema = %s', $tableName, $dbName)
			->orderBy('ORDINAL_POSITION', 'ASC')
			->fetchAll();

		if (!$columns) {
			return;
		}

		//print('<pre>' . print_r($columns, true) .'</pre>');

		$className = $entityName . 'Entity';

		$fileContent = '<?php

namespace ORM;' . "\n";

		// class annotations
		$fileContent .= '
/**
 * Class ' . $className . '
 * @package ORM
 * @database ' . $dbName .'
 * @table ' . $tableName .'
 */' . "\n";

		$fileContent .= 'class ' . $className . ' extends Entity' . "\n";
		$fileContent .= '{';

		foreach ($columns as $column) {
			$type = $column['DATA_TYPE'];
			$columnName = $column['COLUMN_NAME'];

			$fileContent .= '
	/**
	 * @var ' . $type . '
	 * @column ' . $columnName;

			if ($column['COLUMN_KEY'] === 'PRI') {
				$fileContent .= "\n" . '	 * @flags primary';
			}

			$fileContent .= "\n" . '	 */' . "\n";

			$fileContent .= '	public $' . $columnName . ";\n";
		}

		$fileContent .= '}';

		$entityFilePath = Manager::getInstance()->getConfig()->entitiesFolder;
		$entityFileName = $entityFilePath . '/' . $className . '.php';

		if (!file_exists($entityFilePath)) {
			mkdir($entityFilePath, 0777, true);
		}

		file_put_contents($entityFileName, $fileContent);
	}
}