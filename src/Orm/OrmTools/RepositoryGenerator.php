<?php

namespace ORM;

/**
 * Class RepositoryGenerator
 * @package ORM
 */
final class RepositoryGenerator
{
	/**
	 * RepositoryGenerator constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * @param null|string $entityName
	 */
	public function generate($entityName = null)
	{
		if (!empty($entityName)) {
			$this->processEntity($entityName, Manager::getInstance()->getConfig()->repositoriesFolder);

			return;
		}

		// load ORM Entities
		$it = new \RecursiveDirectoryIterator(Manager::getInstance()->getConfig()->entitiesFolder);
		foreach (new \RecursiveIteratorIterator($it) as $filename) {
			$pathInfo = pathinfo($filename);

			$entityName = substr($pathInfo['basename'], 0, strrpos($pathInfo['basename'], 'Entity.php'));
			if (empty($entityName)) {
				continue;
			}

			$dir = $pathInfo['dirname'];
			$entityPos = strrpos($dir, '/Entity\\');
			$dir = substr_replace($dir, '/Repository\\', $entityPos, 8);

			$this->processEntity($entityName, $dir);
		}
	}

	/**
	 * @param string $entityName
	 */
	private function processEntity($entityName, $dir)
	{
		$templateFileName = __DIR__ . '/Templates/TemplateRepository.php';
		$template = file_get_contents($templateFileName);

		// replace the class name
		$newClassName = $entityName . 'Repository';
		$template = str_replace('TemplateRepository', $newClassName, $template);

		// replace the entity name
		$template = str_replace('$this->entityName = \'\';', '$this->entityName = \'' . $entityName . '\';', $template);

		// replace the entity class name
		$newEntityClassName = $entityName . 'Entity';
		$template = str_replace('_Entity', $newEntityClassName, $template);

		$repositoryFileName = $dir . '/' . $newClassName . '.php';

		if (!file_exists($dir)) {
			mkdir($dir, 0777, true);
		}

		file_put_contents($repositoryFileName, $template);
	}
}