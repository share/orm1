<?php

namespace ORM;

/**
 * Class Config
 * @package ORM
 */
final class ConfigTemplate
{
	/** @var string */
	public $entitiesFolder;

	/** @var string */
	public $repositoriesFolder;

	/** @var string */
	public $enumsFolder;

	/** @var string */
	public $dbDriver;

	/** @var string */
	public $dbHost;

	/** @var int */
	public $dbPort;

	/** @var string */
	public $dbUserName;

	/** @var string */
	public $dbPassword;
}