<?php

namespace ORM;

/**
 * Class Config
 * @package ORM
 */
final class Config
{
	/** @var string */
	public $entitiesFolder = './Orm/Entity';

	/** @var string */
	public $repositoriesFolder = './Orm/Repository';

	/** @var string */
	public $enumsFolder = './Orm/Enum';

	/** @var bool */
	public $connectToDatabase = true;

	/** @var string */
	public $dbDriver = 'mysqli';

	/** @var string */
	public $dbHost = 'localhost';

	/** @var int */
	public $dbPort = 3317;

	/** @var string */
	public $dbUserName = 'r3gAdmin';

	/** @var string */
	public $dbPassword = 'r3grouppwd';
}