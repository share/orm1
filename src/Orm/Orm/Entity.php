<?php

namespace ORM;

/**
 * Class Entity
 * @package ORM
 */
abstract class Entity
{
	/** @var array */
	private $defaultValues = [];

	/**
	 * Entity constructor.
	 */
	public function __construct()
	{
		$this->resetDefaultValues();
	}

	/**
	 * @return false|string
	 *
	 * @throws Exception
	 */
	public function serialize()
	{
		try {
			return json_encode($this, JSON_THROW_ON_ERROR);
		} catch (\Exception $ex) {
			throw new Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
		}
	}

	/**
	 *
	 */
	protected function resetDefaultValues()
	{
		$this->defaultValues = [];
		$properties = Utils::getPublicProperties($this);
		foreach ($properties as $propertyName => $propertyValue) {
			$this->defaultValues[$propertyName] = $propertyValue;
		}
	}

	/**
	 * @return array
	 */
	public function getChangedProperties()
	{
		$changes = [];
		foreach ($this->defaultValues as $defaultValueName => $defaultValue) {
			if ($this->$defaultValueName !== $this->defaultValues[$defaultValueName]) {
				$changes[$defaultValueName] = $this->$defaultValueName;
			}
		}

		return $changes;
	}

	/**
	 *
	 */
	public function removeHelperProperties()
	{
		unset($this->defaultValues);
	}
}