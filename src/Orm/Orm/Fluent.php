<?php

namespace ORM;

/**
 * Class Fluent
 * @package ORM
 */
class Fluent extends \Dibi\Fluent
{
	/** @var string */
	protected $dbName;

	/**
	 * Fluent constructor.
	 * @param Connection $connection
	 * @param string $dbName
	 */
	public function __construct(Connection $connection, $dbName)
	{
		parent::__construct($connection);

		$this->dbName = $dbName;
	}

	/**
	 * @param string $clause
	 * @param array $args
	 *
	 * @return \Dibi\Fluent
	 */
	public function __call(string $clause, array $args) : \Dibi\Fluent
	{
		return \Dibi\Fluent::__call($clause, $args);
	}

	/**
	 * @param string $table
	 * @param mixed ...$args
	 *
	 * @return \Dibi\Fluent
	 */
	public function from($table, ...$args)
	{
		return parent::from(Utils::fixDatabaseTableName($this->dbName, $table), ...$args);
	}

	/**
	 * @param mixed ...$table
	 *
	 * @return \Dibi\Fluent
	 */
	public function join(...$table)
	{
		return parent::join(...Utils::fixDatabaseTableName($this->dbName, ...$table));
	}

	/**
	 * @param mixed ...$table
	 *
	 * @return \Dibi\Fluent|void
	 */
	public function leftJoin(...$table)
	{
		return parent::leftJoin(...Utils::fixDatabaseTableName($this->dbName, ...$table));
	}

	/**
	 * @param mixed ...$table
	 *
	 * @return \Dibi\Fluent|void
	 */
	public function innerJoin(...$table)
	{
		return parent::innerJoin(...Utils::fixDatabaseTableName($this->dbName, ...$table));
	}

	/**
	 * @param mixed ...$table
	 *
	 * @return \Dibi\Fluent|void
	 */
	public function outerJoin(...$table)
	{
		return parent::outerJoin(...Utils::fixDatabaseTableName($this->dbName, ...$table));
	}

	/**
	 * @param mixed ...$table
	 *
	 * @return \Dibi\Fluent|void
	 */
	public function rightJoin(...$table)
	{
		return parent::rightJoin(...Utils::fixDatabaseTableName($this->dbName, ...$table));
	}
}