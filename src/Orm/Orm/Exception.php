<?php

namespace ORM;

/**
 * Class Exception
 * @package ORM
 */
class Exception extends \Exception {}