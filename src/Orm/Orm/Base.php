<?php

namespace ORM;

const ORM_NULL = 'NULL';

/**
 * Class ORM_TYPE
 * @package ORM
 */
class ORM_TYPE
{
	const Integer = 'int';
	const Number = 'number';
	const Bool = 'bool';
	const String = 'string';
	const DateTime = 'datetime';
}

/**
 * Class ORM_LIMIT
 * @package ORM
 */
class ORM_LIMIT
{
	const EQUAL = 'equal';
	const LESS = 'less';
	const LESS_EQUAL = 'lessequal';
	const GREATER = 'greater';
	const GREATER_EQUAL = 'greaterequal';
	const ENUM = 'enum';
}

/**
 * Class ORM_ORDER
 * @package ORM
 */
class ORM_ORDER
{
	const ASC = 'ASC';
	const DESC = 'DESC';
}

/**
 * Class ORM_OPERATOR
 * @package ORM
 */
class ORM_OPERATOR
{
	const EQUAL = '=';
	const NOT_EQUAL = 'IS NOT';
	const LARGER_THAN = '>';
	const LARGER_THAN_EQUAL = '>=';
	const SMALLER_THAN = '<';
	const SMALLER_THAN_EQUAL = '<=';
	const IN = 'IN';
	const LIKE = 'LIKE %~like~';
	const LIKE_BEGIN = 'LIKE %like~';
	const LIKE_END = 'LIKE %~like';
}

/**
 * Class ORM_OPTION
 * @package ORM
 */
class ORM_OPTION
{
	const FILTERS = 1;
	const ORDER = 2;
	const OFFSET = 3;
	const LIMIT = 4;
	const MAX_DEPTH = 5;
	const STRICT = 16;
	const NO_TYPE_VALIDATION = 32;
	const NO_REQUIRED_VALIDATION = 64;
	const NO_LIMIT_VALIDATION = 128;
	const FORCE_TYPE_VALIDATION = 256;
	const FORCE_REQUIRED_VALIDATION = 512;
	const FORCE_LIMIT_VALIDATION = 1024;
	const FROM_DATABASE = 2048;
	const MULTIPLE_ROWS = 4096;
	const NO_ACTIVE_FLAG_FILTER = 8192;
}