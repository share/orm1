<?php

namespace ORM;

/**
 * Class Limit
 * @package ORM
 */
class Limit
{
	/** @var string */
	public $type;

	/** @var mixed */
	public $value;

	/**
	 * Limit constructor.
	 * @param string $type
	 * @param mixed $value
	 */
	public function __construct(string $type, $value)
	{
		$this->type = $type;
		$this->value = $value;
	}
}