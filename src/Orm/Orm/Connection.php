<?php

namespace ORM;

use Dibi\Fluent;
use Traversable;

/**
 * Class Connection
 * @package ORM
 */
class Connection extends \Dibi\Connection
{
	/**
	 * @param string $dbName
	 * @param mixed ...$args
	 *
	 * @return Fluent
	 */
	public function makeSelect($dbName, ...$args)
	{
		return $this->makeFluent($dbName)->select(...$args);
	}

	/**
	 * @param string $dbName
	 * @param string $table
	 * @param iterable $args
	 *
	 * @return Fluent
	 */
	public function makeUpdate($dbName, string $table, iterable $args)
	{
		return $this->makeFluent($dbName)->update('%n', $dbName . '.' . $table)->set($args);
	}

	/**
	 * @param string $dbName
	 * @param string $table
	 * @param iterable $args
	 *
	 * @return Fluent
	 */
	public function makeInsert($dbName, string $table, iterable $args)
	{
		if ($args instanceof Traversable) {
			$args = iterator_to_array($args);
		}

		return $this->makeFluent($dbName)->insert()
			->into('%n', $dbName . '.' . $table, '(%n)', array_keys($args))->values('%l', $args);
	}

	/**
	 * @param string $dbName
	 * @param string $table
	 *
	 * @return Fluent
	 */
	public function makeDelete($dbName, string $table)
	{
		return $this->makeFluent($dbName)->delete()->from('%n', $dbName . '.' . $table);
	}

	/**
	 * @param string $dbName
	 *
	 * @return \ORM\Fluent
	 */
	protected function makeFluent($dbName)
	{
		return new \ORM\Fluent($this, $dbName);
	}
}