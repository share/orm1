<?php

namespace ORM;

/**
 * Class FilterGroup
 * @package ORM
 */
class FilterGroup
{
	/** @var int */
	public $index;

	/** @var bool */
	public $isOpen = true;

	/** @var string[] */
	public $filtersHashes = [];

	/**
	 * FilterGroup constructor.
	 * @param int $index
	 */
	public function __construct($index)
	{
		$this->index = $index;
	}
}