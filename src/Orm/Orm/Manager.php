<?php

namespace ORM;

require_once 'Repository.php';

/**
 * Class Manager
 * @package ORM
 */
final class Manager
{
	/** @var Manager */
	private static $instance;

	/** @var ConfigTemplate */
	private $config;

	/** @var bool */
	private $isInitialized = false;

	/** @var Connection */
	private $connection;

	/** @var Repository[] */
	private $repositories = [];

	/** @var array */
	private $limitTypes = [];

	/**
	 * Manager constructor.
	 */
	private function __construct()
	{
		$this->config = new Config();

		$limitTypesReflectionClass = new \ReflectionClass('ORM\\ORM_LIMIT');
		$this->limitTypes = $limitTypesReflectionClass->getConstants();
	}

	/**
	 * @param array $config
	 *
	 * @throws Exception
	 * @throws \Dibi\Exception
	 */
	public function init(array $config)
	{
		if ($this->isInitialized) {
			return;
		}

		$this->isInitialized = true;

		$this->initConfig($config);

		if ($this->config->connectToDatabase) {
			$this->connection = new Connection([
				'driver' => $this->config->dbDriver,
				'host' => $this->config->dbHost,
				'port' => $this->config->dbPort,
				'username' => $this->config->dbUserName,
				'password' => $this->config->dbPassword,
				'result' => [
					'formatDate' => 'Y-m-d',
					'formatDateTime' => 'Y-m-d H:i:s'
				]
			]);
		}

		spl_autoload_register(function($class) {
			_ormRequireClass($class);
		});
	}

	/**
	 * @param array $config
	 *
	 * @throws Exception
	 */
	private function initConfig(array $config)
	{
		$params = get_object_vars($this->config);
		foreach ($params as $paramName => $paramValue) {
			if (!$this->config->connectToDatabase && strpos($paramName, 'db') === 0) {
				continue;
			}

			if (!isset($config[$paramName])) {
				throw new Exception(sprintf('ORM config parameter "%s" not found', $paramName));
			}

			$this->config->$paramName = $config[$paramName];
		}
	}

	/**
	 * @return Manager
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Manager();
		}

		return self::$instance;
	}

	/**
	 * @return ConfigTemplate
	 */
	public function getConfig()
	{
		return $this->config;
	}

	/**
	 * @return Connection
	 */
	public function getDBConnection()
	{
		return $this->connection;
	}

	/**
	 * @param string $entityName
	 *
	 * @return Repository
	 * @throws Exception
	 */
	public function getRepository(string $entityName)
	{
		if (isset($this->repositories[$entityName])) {
			return $this->repositories[$entityName];
		} else {
			$repositoryClassName = 'ORM\\' . $entityName . 'Repository';
			if (!class_exists($repositoryClassName)) {
				throw new Exception(sprintf('Repository class "%s" not found', $repositoryClassName));
			}

			$repository = new $repositoryClassName();
			$this->repositories[$entityName] = $repository;

			return $repository;
		}
	}

	/**
	 * @return array
	 */
	public function getLimitTypes()
	{
		return $this->limitTypes;
	}
}

/**
 * @param string $class
 */
function _ormRequireClass($class)
{
	$classShortName = substr(strrchr($class, "\\"), 1);
	if (strrpos($classShortName, 'Entity') === strlen($classShortName) - 6) {
		$it = new \RecursiveDirectoryIterator(Manager::getInstance()->getConfig()->entitiesFolder);
		foreach (new \RecursiveIteratorIterator($it) as $file) {
			$fileName = basename($file);
			if ($fileName === $classShortName . '.php') {
				require $file;
				return;
			}
		}
	} elseif (strrpos($classShortName, 'Enum') === strlen($classShortName) - 4) {
		$it = new \RecursiveDirectoryIterator(Manager::getInstance()->getConfig()->enumsFolder);
		foreach (new \RecursiveIteratorIterator($it) as $file) {
			$fileName = basename($file);
			if ($fileName === $classShortName . '.php') {
				require $file;
				return;
			}
		}
	} elseif (strrpos($classShortName, 'Repository') === strlen($classShortName) - 10) {
		$it = new \RecursiveDirectoryIterator(Manager::getInstance()->getConfig()->repositoriesFolder);
		foreach (new \RecursiveIteratorIterator($it) as $file) {
			$fileName = basename($file);
			if ($fileName === $classShortName . '.php') {
				require $file;
				return;
			}
		}
	}
}