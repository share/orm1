<?php

namespace ORM;

/**
 * Class BadRequestException
 * @package ORM
 */
class BadRequestException extends Exception
{
	/**
	 * @param string $propertyName
	 * @param string $entityName
	 * @param string $requiredType
	 * @param string $givenType
	 * @param int $rowIndex
	 *
	 * @return BadRequestException
	 */
	public static function wrongTypeException($propertyName, $entityName, $requiredType, $givenType, $rowIndex = -1)
	{
		if ($rowIndex > -1) {
			return new self(sprintf('Property "%s" of entity "%s" must be of type "%s". "%s" given in row "%d"', $propertyName, $entityName, $requiredType, $givenType, $rowIndex));
		} else {
			return new self(sprintf('Property "%s" of entity "%s" must be of type "%s". "%s" given', $propertyName, $entityName, $requiredType, $givenType));
		}
	}

	/**
	 * @param string $propertyName
	 * @param string $entityName
	 * @param mixed $value
	 * @param int $rowIndex
	 *
	 * @return BadRequestException
	 */
	public static function wrongDateFormatException($propertyName, $entityName, $value, $rowIndex = -1)
	{
		if ($rowIndex > -1) {
			return new self(sprintf('Property "%s" of entity "%s" contains malformed DateTime value "%s" in row "%d"', $propertyName, $entityName, $value, $rowIndex));
		} else {
			return new self(sprintf('Property "%s" of entity "%s" contains malformed DateTime value "%s"', $propertyName, $entityName, $value));
		}
	}

	/**
	 * @param string $propertyName
	 * @param string $entityName
	 * @param mixed $limitValue
	 * @param mixed $value
	 * @param string $limitRuleLabel
	 * @param int $rowIndex
	 *
	 * @return BadRequestException
	 */
	public static function limitNotMet($propertyName, $entityName, $limitValue, $value, $limitRuleLabel, $rowIndex = -1)
	{
		if ($rowIndex > -1) {
			return new self(sprintf('Value of property "%s" of entity "%s" must be %s "%s". "%s" given in row "%d"', $propertyName, $entityName, $limitRuleLabel, $limitValue, $value, $rowIndex));
		} else {
			return new self(sprintf('Value of property "%s" of entity "%s" must be %s "%s". "%s" given', $propertyName, $entityName, $limitRuleLabel, $limitValue, $value));
		}
	}

	/**
	 * @param string $propertyName
	 * @param string $entityName
	 * @param string $limitValue
	 * @param mixed $value
	 * @param int $rowIndex
	 *
	 * @return BadRequestException
	 */
	public static function limitNotMetInEnum($propertyName, $entityName, $limitValue, $value, $rowIndex = -1)
	{
		if ($rowIndex > -1) {
			return new self(sprintf('Value of property "%s" of entity "%s" must be one of those listed in the class "%s". "%s" given in row "%d"', $propertyName, $entityName, $limitValue, $value, $rowIndex));
		} else {
			return new self(sprintf('Value of property "%s" of entity "%s" must be one of those listed in the class "%s". "%s" given', $propertyName, $entityName, $limitValue, $value));
		}
	}

	/**
	 * @param string $propertyName
	 * @param string $entityName
	 * @param int $rowIndex
	 *
	 * @return BadRequestException
	 */
	public static function missingRequiredProperty($propertyName, $entityName, $rowIndex = -1)
	{
		if ($rowIndex > -1) {
			return new self(sprintf('Required property "%s" of entity "%s" not specified in row "%d"', $propertyName, $entityName, $rowIndex));
		} else {
			return new self(sprintf('Required property "%s" of entity "%s" not specified', $propertyName, $entityName));
		}
	}
}