<?php

namespace ORM;

/**
 * Class EntityProperty
 * @package ORM
 */
class EntityProperty
{
	const LINKAGE_NONE = 0;
	const LINKAGE_1_1 = 1;
	const LINKAGE_1_N = 2;
	const LINKAGE_N_N = 3;

	/** @var string */
	public $name = '';

	/** @var string */
	public $dbColumn = '';

	/** @var */
	public $type = '';

	/** @var bool */
	public $isPrimary = false;

	/** @var bool */
	public $isActiveFlag = false;

	/** @var bool */
	public $isRequired = false;

	/** @var bool */
	public $isNullable = false;

	/** @var bool */
	public $dontSelect = false;

	/** @var int */
	public $linkage = self::LINKAGE_NONE;

	/** @var string */
	public $linkPropertyName = '';

	/** @var string */
	public $linkTable = '';

	/** @var string */
	public $linkTableLeftColumn = '';

	/** @var string */
	public $linkTableRightColumn = '';

	/** @var Filter[] */
	public $linkTableFilters = [];

	/** @var Limit[] */
	public $limits = [];
}