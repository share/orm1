<?php

namespace ORM;

/**
 * Class Order
 * @package ORM
 */
class Order
{
	/** @var string */
	public $propertyName;

	/** @var string */
	public $dir = ORM_ORDER::ASC;

	/**
	 * Order constructor.
	 * @param string $propertyName
	 * @param string $dir
	 */
	public function __construct($propertyName, $dir = ORM_ORDER::ASC)
	{
		$this->propertyName = $propertyName;
		$this->dir = $dir;
	}
}