<?php

namespace ORM;

/**
 * Class Enum
 * @package ORM
 */
abstract class Enum
{
	/**
	 * @return array
	 * @throws Exception
	 */
	public static function toArray()
	{
		try {
			$reflectionClass = new \ReflectionClass(get_called_class());
		} catch (\ReflectionException $ex) {
			throw new Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
		}

		// extract constants
		$constants = $reflectionClass->getReflectionConstants();
		if (empty($constants)) {
			throw new Exception('The entity class has no property');
		}

		$items = [];
		foreach ($constants as $constant) {
			$constName = $constant->getName();

			try {
				preg_match_all("(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)", $constant->getDocComment(), $matches);
			} catch (\Exception $ex) {
				throw new Exception($ex->getMessage());
			}

			if (!empty($matches[0])) {
				foreach ($matches[0] as $match) {
					if (strpos($match, '@label') !== false) {
						$constName = preg_replace("/^(@\w+\s+)/", "", $match);
						break;
					}
				}
			}

			$constValue = Utils::isInt($constant->getValue()) ? intval($constant->getValue()) : $constant->getValue();

			$items[trim($constValue)] = trim($constName);
		}

		asort($items);

		return $items;
	}
}