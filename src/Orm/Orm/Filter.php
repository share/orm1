<?php

namespace ORM;

/**
 * Class Filter
 * @package ORM
 */
class Filter
{
	/** @var string */
	public $hash;

	/** @var string */
	public $propertyName;

	/** @var mixed */
	public $value;

	/** @var string */
	public $operator = ORM_OPERATOR::EQUAL;

	/**
	 * Filter constructor.
	 * @param $propertyName
	 * @param $value
	 * @param string $operator
	 */
	public function __construct($propertyName, $value, $operator = ORM_OPERATOR::EQUAL)
	{
		$this->hash = substr(md5(uniqid(mt_rand(), true)), 0, 16);
		$this->propertyName = $propertyName;
		$this->value = $value;
		$this->operator = $operator;
	}

	/**
	 * @return bool
	 */
	public function hasLikeOperator()
	{
		return $this->operator === ORM_OPERATOR::LIKE ||
			$this->operator === ORM_OPERATOR::LIKE_BEGIN ||
			$this->operator === ORM_OPERATOR::LIKE_END;
	}
}