<?php

namespace ORM;

use DateTime;
use Dibi\Fluent;
use Dibi\Row;

/**
 * Class Repository
 * @package ORM
 */
abstract class Repository
{
	const OK = 1;
	const FAILURE = 0;
	const ENTITY_NOT_FOUND = -1;
	const NOTHING_TO_DO = -2;

	/** @var Connection */
	protected $dbConnection;

	/** @var string */
	protected $entityName;

	/** @var string */
	protected $entityClassName;

	/** @var string */
	protected $dbName;

	/** @var string */
	protected $dbTableName;

	/** @var EntityProperty[] */
	protected $properties = [];

	/** @var EntityProperty[] */
	protected $overriddenProperties = [];

	/** @var int */
	protected $primaryPropertyIndex = -1;

	/** @var int */
	protected $activeFlagPropertyIndex = -1;

	/** @var array */
	protected $limitEnumValues = [];

	/** @var bool */
	protected $hasBegun = false;

	/** @var int */
	protected $withOptions;

	/** @var array */
	protected $withCustomRequirements = [];

	/** @var mixed */
	protected $withPrimaryValue;

	/** @var Filter[] */
	protected $withFilters = [];

	/** @var FilterGroup[] */
	protected $withFilterGroups = [];

	/** @var string */
	protected $withGroupBy;

	/** @var Order[] */
	protected $withOrders = [];

	/** @var int */
	protected $withOffset = null;

	/** @var int */
	protected $withLimit = null;

	/** @var int */
	protected $withMaxDepth = -1;

	/** @var array */
	protected $withData = [];

	/** @var null|Entity */
	protected $withEntity;

	/** @var Entity[] */
	protected $withEntities = [];

	/** @var int */
	protected $saveResult = self::OK;

	/** @var int */
	protected $lastInsertID = 0;

	/** @var bool */
	protected $enabledDebug = false;

	/** @var string[] */
	protected $latestSQLQueries = [];

	/**
	 * Repository constructor.
	 * @throws Exception
	 */
	protected function __construct()
	{
		$this->entityClassName = 'ORM\\' . $this->entityName . 'Entity';

		$this->dbConnection = Manager::getInstance()->getDBConnection();

		try {
			$reflectionClass = new \ReflectionClass($this->entityClassName);
		} catch (\ReflectionException $ex) {
			throw new Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
		}

		// extract database params
		try {
			preg_match_all("(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)", $reflectionClass->getDocComment(), $dbParams);
		} catch (\Exception $ex) {
			throw new Exception($ex->getMessage());
		}

		if (empty($dbParams[0])) {
			throw new Exception('Failed to extract database parameters');
		}

		foreach ($dbParams[0] as $dbParam) {
			// database name
			if (strpos($dbParam, '@database') !== false) {
				$this->dbName = trim(preg_replace("/^(@\w+\s+)/", '', $dbParam));
				continue;
			}

			// database table name
			if (strpos($dbParam, '@table') !== false) {
				$this->dbTableName = trim(preg_replace("/^(@\w+\s+)/", '', $dbParam));
			}
		}

		// extract properties
		$properties = $reflectionClass->getProperties();
		if (empty($properties)) {
			throw new Exception('The entity class has no property');
		}

		foreach ($properties as $propertyIndex => $property) {
			if (!$property->isPublic()) {
				continue;
			}

			try {
				preg_match_all("(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)", $property->getDocComment(), $matches);
			} catch (\Exception $ex) {
				throw new Exception($ex->getMessage());
			}

			if (empty($matches[0])) {
				continue;
			}

			$entityProperty = new EntityProperty();
			$entityProperty->name = $property->getName();

			foreach ($matches[0] as $match) {
				// type
				if (strpos($match, '@var ') !== false) {
					$entityProperty->type = trim(preg_replace("/^(@\w+\s+)/", "", $match), " \t\n\r\0\x0B[]");
					continue;
				}

				// database table column
				if (strpos($match, '@column ') !== false) {
					$entityProperty->dbColumn = trim(preg_replace("/^(@\w+\s+)/", '', $match));
					continue;
				}

				// flags
				if (strpos($match, '@flags ') !== false) {
					$flagsString = trim(preg_replace("/^(@\w+\s+)/", '', $match));

					try {
						preg_match_all("/[a-zA-Z0-9]+/", $flagsString, $flags);
					} catch (\Exception $ex) {
						throw new Exception($ex->getMessage());
					}

					if (empty($flags[0])) {
						continue;
					}

					foreach ($flags[0] as $flag) {
						if ($flag === 'primary') {
							if ($this->primaryPropertyIndex >= 0) {
								throw new Exception(sprintf('Multiple primary properties for entity "%s" specified. There must be exactly 1', $this->getEntityName()));
							}

							$entityProperty->isPrimary = true;

							$this->primaryPropertyIndex = $propertyIndex;
						} else if($flag === 'activeFlag') {
							if ($this->activeFlagPropertyIndex >= 0) {
								throw new Exception(sprintf('Multiple active flags for entity "%s" specified. Only none or one allowed', $this->getEntityName()));
							}

							if ($entityProperty->type !== ORM_TYPE::Bool) {
								throw new Exception(sprintf('Active flag of entity "%s" must be of type "bool". "%s" given', $this->getEntityName(), $entityProperty->type));
							}

							$entityProperty->isActiveFlag = true;

							$this->activeFlagPropertyIndex = $propertyIndex;
						} elseif ($flag === 'required') {
							$entityProperty->isRequired = true;
						} elseif ($flag === 'nullable') {
							$entityProperty->isNullable = true;
						} elseif ($flag === 'dontSelect') {
							$entityProperty->dontSelect = true;
						} elseif ($flag === '1to1') {
							$entityProperty->linkage = EntityProperty::LINKAGE_1_1;
						} elseif ($flag === '1toN') {
							$entityProperty->linkage = EntityProperty::LINKAGE_1_N;
						} elseif ($flag === 'NtoN') {
							$entityProperty->linkage = EntityProperty::LINKAGE_N_N;
						}
					}
				}

				// linking column
				if (strpos($match, '@link ') !== false) {
					$entityProperty->linkPropertyName = trim(preg_replace("/^(@\w+\s+)/", '', $match));
					continue;
				}

				// linking table
				if (strpos($match, '@linkTable ') !== false) {
					$entityProperty->linkTable = trim(preg_replace("/^(@\w+\s+)/", '', $match));
					continue;
				}

				// linking table left column
				if (strpos($match, '@linkTableLeftColumn ') !== false) {
					$entityProperty->linkTableLeftColumn = trim(preg_replace("/^(@\w+\s+)/", '', $match));
					continue;
				}

				// linking table right column
				if (strpos($match, '@linkTableRightColumn ') !== false) {
					$entityProperty->linkTableRightColumn = trim(preg_replace("/^(@\w+\s+)/", '', $match));
					continue;
				}

				// linking table filters
				if (strpos($match, '@linkTableFilter ') !== false) {
					$filter = trim(preg_replace("/^(@\w+\s+)/", '', $match));

					$operators = sprintf('@([%s])@', preg_quote('<>='));
					$parts = preg_split($operators, preg_replace('@\s@', '', $filter), -1, PREG_SPLIT_DELIM_CAPTURE);

					if (count($parts) !== 3) {
						throw new Exception(sprintf('Malformed linking table filter for property "%s" of entity "%s" specified', $entityProperty->name, $this->getEntityName()));
					}

					if ($parts[1] === ORM_OPERATOR::IN) {
						throw new Exception(sprintf('Operator IN not supported in linking table filters. Property "%s" of entity "%s"', $entityProperty->name, $this->getEntityName()));
					}

					$entityProperty->linkTableFilters[] = new Filter($parts[0], $parts[2], $parts[1]);
				}

				// limits
				if (strpos($match, '@limit ') !== false) {
					$limitsString = trim(preg_replace("/^(@\w+\s+)/", '', $match));

					try {
						preg_match_all("/[a-zA-Z0-9]+/", $limitsString, $limits);
					} catch (\Exception $ex) {
						throw new Exception($ex->getMessage());
					}

					if (empty($limits[0]) || count($limits[0]) !== 2) {
						continue;
					}

					$limitType = $limits[0][0];
					$limitValue = $limits[0][1];

					if (!in_array($limitType, Manager::getInstance()->getLimitTypes())) {
						throw new Exception(sprintf('Unknown limit type "%s" in entity "%s"', $limitType, $this->entityName));
					}

					if ($limitType === ORM_LIMIT::ENUM) {
						$enumClassName = 'ORM\\' . $limitValue;

						if (!class_exists($enumClassName)) {
							throw new Exception(sprintf('Unknown class "%s" specified as an enum limit value in entity "%s"', $enumClassName, $this->entityName));
						}

						if (!isset($this->limitEnumValues[$limitValue])) {
							try {
								$enumReflectionClass = new \ReflectionClass($enumClassName);
							} catch (\ReflectionException $ex) {
								throw new Exception($ex->getMessage(), $ex->getCode(), $ex->getPrevious());
							}

							$this->limitEnumValues[$limitValue] = $enumReflectionClass->getConstants();
						}
					}

					$limit = new Limit($limitType, $limitValue);

					$entityProperty->limits[] = $limit;
				}
			}

			if (empty($entityProperty->type)) {
				throw new Exception(sprintf('Type not specified for Property "%s" of entity "%s', $property->getName(), $this->entityName));
			}

			$this->properties[$propertyIndex] = $entityProperty;
		}

		$this->postConstructCheck();
	}

	/**
	 * @throws Exception
	 */
	private function postConstructCheck()
	{
		// properties
		foreach ($this->properties as $property) {
			if ($property->linkage === EntityProperty::LINKAGE_1_1) {
				if (!class_exists('ORM\\' . $property->type)) {
					throw new Exception(sprintf('Entity of class "%s" linked by property "%s" of entity "%s" not found', 'ORM\\' . $property->type, $property->name, $this->getEntityName()));
				}
			} elseif ($property->linkage === EntityProperty::LINKAGE_1_N) {
				if (!class_exists('ORM\\' . $property->type)) {
					throw new Exception(sprintf('Entity of class "%s" linked by property "%s" of entity "%s" not found', 'ORM\\' . $property->type, $property->name, $this->getEntityName()));
				}

				if (empty($property->linkPropertyName)) {
					throw new Exception(sprintf('Link for property "%s" of entity "%s" not specified', $property->name, $this->getEntityName()));
				}
			} elseif ($property->linkage === EntityProperty::LINKAGE_N_N) {
				if (!class_exists('ORM\\' . $property->type)) {
					throw new Exception(sprintf('Entity of class "%s" linked by property "%s" of entity "%s" not found', 'ORM\\' . $property->type, $property->name, $this->getEntityName()));
				}

				if (empty($property->linkTable)) {
					throw new Exception(sprintf('Linking table for property "%s" of entity "%s" not specified', $property->name, $this->getEntityName()));
				}

				if (empty($property->linkTableLeftColumn)) {
					throw new Exception(sprintf('Left linking column for property "%s" of entity "%s" not specified', $property->name, $this->getEntityName()));
				}

				if (empty($property->linkTableRightColumn)) {
					throw new Exception(sprintf('Right linking column for property "%s" of entity "%s" not specified', $property->name, $this->getEntityName()));
				}
			}
		}
	}

	/**
	 * @return string
	 */
	public function getEntityName()
	{
		return $this->entityName;
	}

	/**
	 * @return string
	 */
	public function getEntityClassName()
	{
		return $this->entityClassName;
	}

	/**
	 * @return Connection
	 */
	public function getDatabaseConnection()
	{
		return $this->dbConnection;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getDatabaseName()
	{
		if (empty($this->dbName)) {
			throw new Exception(sprintf('Database name for entity "%s" not specified', $this->entityName));
		}

		return $this->dbName;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	public function getDatabaseTableName()
	{
		if (empty($this->dbTableName)) {
			throw new Exception(sprintf('Database table name for entity "%s" not specified', $this->entityName));
		}

		return $this->dbTableName;
	}

	/**
	 * @return EntityProperty[]
	 */
	public function getProperties()
	{
		return $this->properties;
	}

	/**
	 * @return EntityProperty
	 * @throws Exception
	 */
	public function getPrimaryProperty()
	{
		if (!isset($this->properties[$this->primaryPropertyIndex])) {
			throw new Exception(sprintf('The primary property for entity "%s" not found', $this->entityName));
		}

		return $this->properties[$this->primaryPropertyIndex];
	}

	/**
	 * @return EntityProperty|null
	 */
	public function getActiveFlagProperty()
	{
		if (!isset($this->properties[$this->activeFlagPropertyIndex])) {
			return null;
		}

		return $this->properties[$this->activeFlagPropertyIndex];
	}

	/**
	 * @param string $name
	 *
	 * @return EntityProperty|null
	 */
	public function getPropertyByName(string $name)
	{
		foreach ($this->overriddenProperties as $overriddenProperty) {
			if ($overriddenProperty->name === $name) {
				return $overriddenProperty;
			}
		}

		foreach ($this->properties as $property) {
			if ($property->name === $name) {
				return $property;
			}
		}

		return null;
	}

	/**
	 * @param string $name
	 *
	 * @return EntityProperty|null
	 */
	public function getPropertyByDBColumnName(string $name)
	{
		foreach ($this->overriddenProperties as $overriddenProperty) {
			if ($overriddenProperty->dbColumn === $name) {
				return $overriddenProperty;
			}
		}

		foreach ($this->properties as $property) {
			if ($property->dbColumn === $name) {
				return $property;
			}
		}

		return null;
	}

	/**
	 * @param bool $enable
	 */
	public function enableDebug(bool $enable)
	{
		$this->enabledDebug = $enable;
	}

	/**
	 * @return $this
	 */
	public function begin()
	{
		$this->hasBegun = true;

		$this->overriddenProperties = [];

		$this->withOptions = null;
		$this->withCustomRequirements = [];
		$this->withPrimaryValue = null;
		$this->withFilters = [];
		$this->withGroupBy = null;
		$this->withOrders = [];
		$this->withOffset = null;
		$this->withLimit = null;
		$this->withMaxDepth = -1;
		$this->withData = [];
		$this->withEntity = null;
		$this->withEntities = [];

		$this->latestSQLQueries = [];

		return $this;
	}

	/**
	 * @param EntityProperty $property
	 *
	 * @return $this
	 */
	public function withOverriddenProperty(EntityProperty $property)
	{
		foreach ($this->overriddenProperties as $index => $overriddenProperty) {
			if ($overriddenProperty->name === $property->name) {
				$this->overriddenProperties[$index] = $property;
				return $this;
			}
		}

		$this->overriddenProperties[] = $property;

		return $this;
	}

	/**
	 * @param null $options
	 *
	 * @return $this
	 */
	public function withOptions($options = null)
	{
		$this->withOptions = $options;

		return $this;
	}

	/**
	 * @param array $customRequirements
	 *
	 * @return $this
	 */
	public function withCustomRequirements(array $customRequirements = [])
	{
		$this->withCustomRequirements = $customRequirements;

		return $this;
	}

	/**
	 * @param mixed $value
	 *
	 * @return $this
	 */
	public function withPrimaryValue($value)
	{
		$this->withPrimaryValue = $value;

		return $this;
	}

	/**
	 * @param string $propertyName
	 * @param mixed $value
	 * @param string $operator
	 *
	 * @return $this
	 */
	public function withFilter(string $propertyName, $value, $operator = ORM_OPERATOR::EQUAL)
	{
		$filter = new Filter($propertyName, $value, $operator);
		if (!in_array($filter, $this->withFilters)) {
			$this->withFilters[] = $filter;

			if (!empty($this->withFilterGroups)) {
				$latestGroup = $this->withFilterGroups[count($this->withFilterGroups) - 1];
				if ($latestGroup->isOpen) {
					$latestGroup->filtersHashes[] = $filter->hash;
				}
			}
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function withFilterGroupBegin()
	{
		$group = new FilterGroup(count($this->withFilterGroups));
		$this->withFilterGroups[] = $group;

		return $this;
	}

	/**
	 * @return $this
	 */
	public function withFilterGroupEnd()
	{
		$this->withFilterGroups[count($this->withFilterGroups) - 1]->isOpen = false;

		return $this;
	}

	/**
	 * @param string $propertyName
	 *
	 * @return $this
	 */
	public function withGroupBy($propertyName)
	{
		$this->withGroupBy = $propertyName;

		return $this;
	}

	/**
	 * @param string $propertyName
	 * @param string $dir
	 *
	 * @return $this
	 */
	public function withOrder(string $propertyName, $dir = ORM_ORDER::ASC)
	{
		$order = new Order($propertyName, $dir);
		if (!in_array($order, $this->withOrders)) {
			$this->withOrders[] = $order;
		}

		return $this;
	}

	/**
	 * @param int $offset
	 *
	 * @return $this
	 */
	public function withOffset(int $offset)
	{
		$this->withOffset = $offset;

		return $this;
	}

	/**
	 * @param int $limit
	 *
	 * @return $this
	 */
	public function withLimit(int $limit)
	{
		$this->withLimit = $limit;

		return $this;
	}

	/**
	 * @param int $maxDepth
	 *
	 * @return $this
	 */
	public function withMaxDepth(int $maxDepth)
	{
		$this->withMaxDepth = $maxDepth;

		return $this;
	}

	/**
	 * @param array $data
	 *
	 * @return $this
	 */
	public function withData(array $data)
	{
		$this->withData = $data;

		return $this;
	}

	/**
	 * @param mixed $entity
	 *
	 * @return $this
	 */
	public function withEntity($entity)
	{
		$this->withEntity = $entity;

		return $this;
	}

	/**
	 * @param array $entities
	 *
	 * @return $this
	 */
	public function withEntities(array $entities)
	{
		$this->withEntities = $entities;

		return $this;
	}

	/**
	 * @return string[]
	 */
	public function getLatestSQLQueries()
	{
		return $this->latestSQLQueries;
	}

	/**
	 * @return array|mixed
	 * @throws BadRequestException
	 * @throws Exception
	 */
	protected function populateEntity()
	{
		if (empty($this->withData)) {
			throw new Exception(sprintf('No data to populate entity "%s" specified', $this->entityName));
		}

		if ($this->withOptions & ORM_OPTION::MULTIPLE_ROWS) {
			$entities = [];

			foreach ($this->withData as $index => $row) {
				$entities[] = $this->populateEntityInternal($this, (array)$row, $this->withOptions, $this->withCustomRequirements, $index);
			}

			return $entities;
		} else {
			return $this->populateEntityInternal($this, $this->withData, $this->withOptions, $this->withCustomRequirements);
		}
	}

	/**
	 * @param Repository $repository
	 * @param array $data
	 * @param null|int $options
	 * @param array $customRequirements
	 * @param int $rowIndex
	 *
	 * @return mixed
	 * @throws BadRequestException
	 * @throws Exception
	 */
	private function populateEntityInternal(Repository $repository, array $data, $options = null, array $customRequirements = [], int $rowIndex = -1)
	{
		$entityClassName = $repository->getEntityClassName();
		$entity = new $entityClassName();

		// allow only properties that actually exists in the entity's class
		foreach ($data as $key => $value) {
			$propertyName = $key;

			if ($options & ORM_OPTION::FROM_DATABASE) {
				$property = $repository->getPropertyByDBColumnName($key);

				if (!$property && $options & ORM_OPTION::STRICT) {
					if ($rowIndex > -1) {
						throw new Exception(sprintf('Database column "%s" provided in row %d is not associated with any property of entity "%s"', $key, $rowIndex, $repository->getEntityName()));
					} else {
						throw new Exception(sprintf('Database column "%s" is not associated with any property of entity "%s"', $key, $repository->getEntityName()));
					}
				}

				if ($property) {
					$propertyName = $property->name;
				} else {
					unset($key);
				}
			}

			if (!property_exists($entityClassName, $propertyName)) {
				if ($options & ORM_OPTION::STRICT) {
					if ($rowIndex > -1) {
						throw new Exception(sprintf('Property "%s" provided in row %d does not exist in entity "%s"', $propertyName, $rowIndex, $repository->getEntityName()));
					} else {
						throw new Exception(sprintf('Property "%s" does not exist in entity "%s"', $propertyName, $repository->getEntityName()));
					}
				} else {
					unset($data[$propertyName]);
				}
			}
		}

		// verify the presence of all required properties
		if (!($options & ORM_OPTION::NO_REQUIRED_VALIDATION)) {
			$repository->validateRequiredValues($data, $customRequirements, $options & ORM_OPTION::FROM_DATABASE, $rowIndex);
		}

		// validate data types
		if (!($options & ORM_OPTION::NO_TYPE_VALIDATION)) {
			$repository->validateDataTypes($data, $options & ORM_OPTION::FROM_DATABASE, $rowIndex);
		}

		// validate limited values
		if (!($options & ORM_OPTION::NO_LIMIT_VALIDATION)) {
			$repository->validateLimitedValues($data, $options & ORM_OPTION::FROM_DATABASE, $rowIndex);
		}

		foreach ($data as $key => $value) {
			if ($options & ORM_OPTION::FROM_DATABASE) {
				$property = $repository->getPropertyByDBColumnName($key);
				if (!$property) {
					if ($rowIndex > -1) {
						throw new Exception(sprintf('Database column "%s" provided in row %d is not associated with any property in entity "%s"', $key, $rowIndex, $repository->getEntityName()));
					} else {
						throw new Exception(sprintf('Database column "%s" is not associated with any property in entity "%s"', $key, $repository->getEntityName()));
					}
				}

				$key = $property->name;
			} else {
				$property = $repository->getPropertyByName($key);
			}

			if ($repository->isPropertyTypeBasic($property) || Utils::isInt($value)) {
				$entity->$key = $value;
			} elseif (is_array($value)) {
				$childRepository = Manager::getInstance()->getRepository($repository->getBarePropertyTypeName($property));

				if ($property->linkage === EntityProperty::LINKAGE_1_1) {
					$entity->$key = $this->populateEntityInternal($childRepository, $value, $options, [], $rowIndex);
				} elseif ($property->linkage === EntityProperty::LINKAGE_1_N || $property->linkage === EntityProperty::LINKAGE_N_N) {
					foreach ($value as $childEntity) {
						if (empty($childEntity) || !is_array($childEntity)) {
							continue;
						}

						$entity->$key[] = $this->populateEntityInternal($childRepository, $childEntity, $options, [], $rowIndex);
					}
				}
			}
		}

		return $entity;
	}

	/**
	 * @param array $data
	 * @param bool $useDBPropertyNames
	 * @param int $rowIndex
	 *
	 * @throws Exception
	 */
	public function validateDataTypes(array $data, bool $useDBPropertyNames = false, int $rowIndex = -1)
	{
		foreach ($data as $key => $value) {
			$property = $useDBPropertyNames ? $this->getPropertyByDBColumnName($key) : $this->getPropertyByName($key);
			if (!$property) {
				throw new BadRequestException(sprintf('Property "%s" of entity "%s" not found', $key, $this->entityName));
			}

			if (is_null($value)) {
				continue;
			}

			if ($property->type === ORM_TYPE::Integer) {
				if (!Utils::isInt($value)) {
					throw BadRequestException::wrongTypeException($key, $this->entityName, 'int', gettype($value), $rowIndex);
				}
			} elseif ($property->type === ORM_TYPE::Number) {
				if (!is_numeric($value)) {
					throw BadRequestException::wrongTypeException($key, $this->entityName, 'number', gettype($value), $rowIndex);
				}
			} elseif ($property->type === ORM_TYPE::Bool) {
				if (!Utils::isBool($value)) {
					throw BadRequestException::wrongTypeException($key, $this->entityName, 'bool', gettype($value), $rowIndex);
				}
			} elseif ($property->type === ORM_TYPE::String) {
				if (!is_string($value)) {
					throw BadRequestException::wrongTypeException($key, $this->entityName, 'string', gettype($value), $rowIndex);
				}
			} elseif ($property->type === ORM_TYPE::DateTime) {
				if (DateTime::createFromFormat('Y-m-d', $value) === false &&
					DateTime::createFromFormat('Y-m-d H:i:s', $value) === false &&
					DateTime::createFromFormat('Y-m-d H:i:s.u', $value) === false &&
					DateTime::createFromFormat('Y-m-d H:i:s.v', $value) === false) {
					throw BadRequestException::wrongDateFormatException($key, $this->entityName, $value, $rowIndex);
				}
			} else {
				if (($property->linkage === EntityProperty::LINKAGE_1_1 || $property->linkage === EntityProperty::LINKAGE_N_N) &&
					!is_array($value)) {
					if (is_object($value)) {
						if ('ORM\\' . $property->type !== get_class($value)) {
							throw BadRequestException::wrongTypeException($key, $this->entityName, $property->type, get_class($value), $rowIndex);
						}
					} else {
						if (!Utils::isInt($value)) {
							throw BadRequestException::wrongTypeException($key, $this->entityName, 'int', gettype($value), $rowIndex);
						}
					}
				}
			}
		}
	}

	/**
	 * @param array $data
	 * @param array $customRequirements
	 * @param bool $useDBPropertyNames
	 * @param int $rowIndex
	 *
	 * @throws BadRequestException
	 */
	public function validateRequiredValues(array $data, array $customRequirements = [], bool $useDBPropertyNames = false, int $rowIndex = -1)
	{
		foreach ($this->getProperties() as $property) {
			$propertyName = $useDBPropertyNames ? $property->dbColumn : $property->name;

			$isRequired = $property->isRequired;
			if (!empty($customRequirements)) {
				$isRequired = in_array($property->name, $customRequirements);
			}

			if (!$isRequired) {
				continue;
			}

			if (!isset($data[$propertyName])) {
				throw BadRequestException::missingRequiredProperty($propertyName, $this->entityName, $rowIndex);
			}

			if (is_object($data[$propertyName]) || (is_array($data[$propertyName]) && count($data[$propertyName]))) {
				continue;
			}

			if ((!is_object($data[$propertyName]) && strlen(trim($data[$propertyName])) <= 0 && $data[$propertyName] !== false)) {
				throw BadRequestException::missingRequiredProperty($propertyName, $this->entityName, $rowIndex);
			}
		}
	}

	/**
	 * @param array $data
	 * @param bool $useDBPropertyNames
	 * @param int $rowIndex
	 *
	 * @throws BadRequestException
	 * @throws Exception
	 */
	public function validateLimitedValues(array $data, bool $useDBPropertyNames = false, int $rowIndex = -1)
	{
		foreach ($this->getProperties() as $property) {
			if (empty($property->limits)) {
				continue;
			}

			if ($property->linkage === EntityProperty::LINKAGE_N_N) {
				continue;
			}

			$propertyName = $useDBPropertyNames ? $property->dbColumn : $property->name;

			$value = $data[$propertyName];
			if (empty($value)) {
				continue;
			}

			foreach ($property->limits as $limit) {
				if ($limit->type === ORM_LIMIT::EQUAL) {
					if ($value != $limit->value) {
						throw BadRequestException::limitNotMet($propertyName, $this->entityName, $limit->value, $value, 'equal to', $rowIndex);
					}
				} elseif ($limit->type === ORM_LIMIT::LESS) {
					if ($value >= $limit->value) {
						throw BadRequestException::limitNotMet($propertyName, $this->entityName, $limit->value, $value, 'less than', $rowIndex);
					}
				} elseif ($limit->type === ORM_LIMIT::LESS_EQUAL) {
					if ($value > $limit->value) {
						throw BadRequestException::limitNotMet($propertyName, $this->entityName, $limit->value, $value, 'less or equal to', $rowIndex);
					}
				} elseif ($limit->type === ORM_LIMIT::GREATER) {
					if ($value <= $limit->value) {
						throw BadRequestException::limitNotMet($propertyName, $this->entityName, $limit->value, $value, 'greater than', $rowIndex);
					}
				} elseif ($limit->type === ORM_LIMIT::GREATER_EQUAL) {
					if ($value < $limit->value) {
						throw BadRequestException::limitNotMet($propertyName, $this->entityName, $limit->value, $value, 'greater equal to', $rowIndex);
					}
				} elseif ($limit->type === ORM_LIMIT::ENUM) {
					if (!is_array($value)) {
						if (!$this->isPropertyTypeBasic($property) && !Utils::isInt($value)) {
							if ($property->linkage !== EntityProperty::LINKAGE_1_1) {
								continue;
							}

							$childRepository = Manager::getInstance()->getRepository($this->getBarePropertyTypeName($property));
							$value = $childRepository->getEntityPrimaryValue($value);
						}

						if (!in_array($value, $this->limitEnumValues[$limit->value])) {
							throw BadRequestException::limitNotMetInEnum($propertyName, $this->entityName, $limit->value, $value, $rowIndex);
						}
					}
				}
			}
		}
	}

	/**
	 * @return mixed
	 * @throws Exception
	 */
	protected function getAll()
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		return $this->getEntities($this, [], $this->withGroupBy, $this->withOrders, $this->withOptions, $this->withOffset, $this->withLimit, $this->withMaxDepth);
	}

	/**
	 * @return mixed
	 * @throws Exception
	 */
	protected function getAllBy()
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		return $this->getEntities($this, $this->withFilters, $this->withGroupBy, $this->withOrders, $this->withOptions, $this->withOffset, $this->withLimit, $this->withMaxDepth);
	}

	/**
	 * @return mixed
	 * @throws Exception
	 */
	protected function getOne()
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		if (!isset($this->withPrimaryValue) && empty($this->withFilters)) {
			throw new Exception(sprintf('Neither primary key value nor filters have been specified for entity "%s"', $this->entityName));
		}

		$primaryProperty = $this->getPrimaryProperty();
		if (isset($this->withPrimaryValue)) {
			$filters = array_merge($this->withFilters, [new Filter($primaryProperty->name, $this->withPrimaryValue)]);
		} else {
			$filters = $this->withFilters;
		}

		$entities = $this->getEntities($this,
			$filters, null, [], $this->withOptions, 0, 1,
			$this->withMaxDepth);

		return isset($entities[0]) ? $entities[0] : null;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	protected function doesEntityExist()
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		if (!isset($this->withPrimaryValue) && empty($this->withFilters)) {
			throw new Exception(sprintf('Neither primary key value nor filters have been specified for entity "%s"', $this->entityName));
		}

		$primaryProperty = $this->getPrimaryProperty();
		if (isset($this->withPrimaryValue)) {
			$filters = array_merge($this->withFilters, [new Filter($primaryProperty->name, $this->withPrimaryValue)]);
		} else {
			$filters = $this->withFilters;
		}

		return $this->doesEntityExistInternal($filters);
	}

	/**
	 * @param array $filters
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function doesEntityExistInternal(array $filters = [])
	{
		$query = $this->dbConnection->select($this->getPrimaryProperty()->dbColumn)
			->from(sprintf('%s.%s', $this->getDatabaseName(), $this->getDatabaseTableName()));

		foreach ($filters as $filter) {
			if (!isset($filter->value)) {
				throw new Exception('Filters cannot have empty value');
			}

			$filterProperty = $this->getPropertyByName($filter->propertyName);
			if (!$filterProperty) {
				throw new Exception(sprintf('Property of name "%s" not found', $filter->propertyName));
			}

			if ($filter->operator !== ORM_OPERATOR::IN) {
				$query->where(sprintf('%s %s "%s"', $filterProperty->dbColumn, $filter->operator, $filter->value));
			} elseif ($filter->operator === ORM_OPERATOR::IN) {
				if (!is_array($filter->value)) {
					throw new Exception('The IN operator requires an array as the parameter');
				}

				$query->where(sprintf('%s IN(%s)', $filterProperty->dbColumn, trim(implode(',', $filter->value), " \t\n\r \v,")));
			}
		}

		try {
			$res = $query->fetch();
		} catch (\Dibi\Exception $ex) {
			throw new Exception($ex->getMessage() . ' | ' . $ex->getSql() . ' | ', 500, $ex->getPrevious());
		}

		return (bool)$res;
	}

	/**
	 * @param mixed ...$args
	 *
	 * @return Fluent
	 * @throws Exception
	 */
	protected function makeSelect(...$args)
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		return $this->dbConnection->makeSelect($this->getDatabaseName(), ...$args);
	}

	/**
	 * @param string $table
	 * @param iterable $args
	 *
	 * @return Fluent
	 * @throws Exception
	 */
	protected function makeUpdate(string $table, iterable $args)
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		return $this->dbConnection->makeUpdate($this->getDatabaseName(), $table, $args);
	}

	/**
	 * @param string $table
	 * @param iterable $args
	 *
	 * @return Fluent
	 * @throws Exception
	 */
	protected function makeInsert(string $table, iterable $args)
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		return $this->dbConnection->makeInsert($this->getDatabaseName(), $table, $args);
	}

	/**
	 * @param string $table
	 *
	 * @return Fluent
	 * @throws Exception
	 */
	protected function makeDelete(string $table)
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		return $this->dbConnection->makeDelete($this->getDatabaseName(), $table);
	}

	/**
	 * @param Repository $repository
	 * @param Filter[] $filters
	 * @param null|string $groupBy
	 * @param Order[] $orders
	 * @param int|null $options
	 * @param int|null $offset
	 * @param int|null $limit
	 * @param int $maxDepth
	 * @param int $depth
	 *
	 * @return array|Entity
	 * @throws Exception
	 */
	protected function getEntities(Repository $repository, array $filters = [], $groupBy = null, array $orders = [], $options = null, $offset = null, $limit = null, $maxDepth = -1, $depth = 0)
	{
		$query = $this->composeSelect(new \ORM\Fluent($this->dbConnection, $this->getDatabaseName()), $repository)
			->from(sprintf('%s.%s', $repository->getDatabaseName(), $repository->getDatabaseTableName()));

		// prepare user filters
		$applicableFilters = $this->getApplicableFilters($filters, $depth);

		$simpleFilters = [];
		$orFilters = [];
		foreach ($applicableFilters as $filter) {
			$filterGroup = $this->getFilterParentGroup($filter);
			if (!$filterGroup) {
				$simpleFilters[] = $filter;
			} else {
				$filterProperty = $repository->getPropertyByName($filter->propertyName);
				if (!$filterProperty) {
					throw new Exception(sprintf('Property of name "%s" not found', $filter->propertyName));
				}

				if (!isset($filter->value)) {
					throw new Exception('Filters cannot have empty value');
				}

				$orFilters[$filterGroup->index][] = [
					$filterProperty->dbColumn . ' ' . $filter->operator . ($filter->hasLikeOperator() ? '' : ' ?'),
					$filter->value
				];
			}
		}

		// apply simple user filters
		foreach ($simpleFilters as $filter) {
			if (!isset($filter->value)) {
				throw new Exception('Filters cannot have empty value');
			}

			$filterProperty = $repository->getPropertyByName($filter->propertyName);
			if (!$filterProperty) {
				throw new Exception(sprintf('Property of name "%s" not found', $filter->propertyName));
			}

			if ($filter->operator !== ORM_OPERATOR::IN) {
				if ($filter->operator === ORM_OPERATOR::LIKE || $filter->operator === ORM_OPERATOR::LIKE_BEGIN || $filter->operator === ORM_OPERATOR::LIKE_END) {
					$query->where(sprintf('%s %s', $filterProperty->dbColumn, $filter->operator), $filter->value);
				} else {
					$query->where('%n ' . $filter->operator . ' ?', $filterProperty->dbColumn, $filter->value);
				}
			} elseif ($filter->operator === ORM_OPERATOR::IN) {
				if (!is_array($filter->value)) {
					throw new Exception('The IN operator requires an array as the parameter');
				}

				$query->where('%n IN(?)', $filterProperty->dbColumn, $filter->value);
			}
		}

		// apply OR user filters
		foreach ($orFilters as $filter) {
			$query->where('(%or)', array_values($filter));
		}

		// the 'active flag' filter
		if (!($options & ORM_OPTION::NO_ACTIVE_FLAG_FILTER)) {
			$activeFlagProperty = $repository->getActiveFlagProperty();
			if ($activeFlagProperty) {
				$query->where(sprintf('%s = 1', $activeFlagProperty->dbColumn));
			}
		}

		// grouping
		if (!empty($groupBy) && $limit !== 1) {
			$groupingProperty = $repository->getPropertyByName($groupBy);
			$query->groupBy($groupingProperty->dbColumn);
		}

		// ordering
		if (!empty($orders)) {
			foreach ($orders as $order) {
				$orderProperty = $repository->getPropertyByName($order->propertyName);
				$query->orderBy($orderProperty->dbColumn, $order->dir);
			}
		}

		if ($this->enabledDebug) {
			$this->latestSQLQueries[] = $repository->getEntityName() . ' | ' . $query->__toString();
		}

		// fetch entries
		try {
			$data = $query->fetchAll($offset, $limit);
		} catch (\Dibi\Exception $ex) {
			throw new Exception($ex->getMessage() . ' | ' . $ex->getSql() . ' | ', 500, $ex->getPrevious());
		}

		$entities = [];
		if ($data) {
			if ($data instanceof Row) {
				$entities[] = $this->createEntity($repository, $data);
			} else {
				foreach ($data as $row) {
					$entities[] = $this->createEntity($repository, $row);
				}
			}
		}

		// another entity's property points to this entity's primary property (1toN, NtoN)
		$entityPrimaries = [];
		// a property of this entity points to another entity's primary property (1to1)
		$entitySecondaries = [];
		foreach ($entities as $entity) {
			$entityPrimaryValue = $repository->getEntityPrimaryValue($entity);
			if (!empty($entityPrimaryValue)) {
				$entityPrimaries[] = $entityPrimaryValue;
			}

			foreach ($repository->getProperties() as $property) {
				if (!$this->isPropertyTypeBasic($property)) {
					$propertyName = $property->name;
					if (isset($entitySecondaries[$propertyName]) && in_array($entity->$propertyName, $entitySecondaries[$propertyName])) {
						continue;
					}

					if (!empty($entity->$propertyName)) {
						$entitySecondaries[$propertyName][] = $entity->$propertyName;
					}
				}
			}
		}

		// process child entities if the max depth hasn't been reached yet
		if ($depth + 1 <= $maxDepth || $maxDepth === -1) {
			// foreach non-basic property, try to find its children
			foreach ($repository->getProperties() as $property) {
				if (!$this->isPropertyTypeBasic($property) && !$property->dontSelect) {
					$propertyName = $property->name;

					$childEntityRepository = Manager::getInstance()->getRepository($this->getBarePropertyTypeName($property));
					$childLinkingProperty = $childEntityRepository->getPropertyByName($property->linkPropertyName);

					$childEntityFilters = $this->getFiltersForChildEntity($propertyName, $filters, $depth);

					if ($property->linkage === EntityProperty::LINKAGE_1_1) {
						if (!empty($entitySecondaries[$property->name])) {
							$childLinkingProperty = $childEntityRepository->getPrimaryProperty();

							$childEntityFilters[] = new Filter($childLinkingProperty->name, $entitySecondaries[$property->name], ORM_OPERATOR::IN);
							$childEntities = $this->getEntities($childEntityRepository, $childEntityFilters, null, [], $options, null, null, $maxDepth, $depth + 1);
						}
					} elseif ($property->linkage === EntityProperty::LINKAGE_1_N) {
						if (!empty($entityPrimaries)) {
							$childEntityFilters[] = new Filter($childLinkingProperty->name, $entityPrimaries, ORM_OPERATOR::IN);
							$childEntities = $this->getEntities($childEntityRepository, $childEntityFilters, null, [], $options, null, null, $maxDepth, $depth + 1);
						}
					} elseif ($property->linkage === EntityProperty::LINKAGE_N_N) {
						if (!empty($entityPrimaries)) {
							// load child entity IDs
							try {
								$query = $this->dbConnection->select(sprintf('%s, %s', $property->linkTableLeftColumn, $property->linkTableRightColumn))
									->from(sprintf('%s.%s', $repository->getDatabaseName(), $property->linkTable))
									->where(sprintf('%s IN(%s)', $property->linkTableLeftColumn, trim(implode(',', $entityPrimaries), " \t\n\r \v,")));

								if (!empty($property->linkTableFilters)) {
									foreach ($property->linkTableFilters as $filter) {
										$query->where(sprintf('%s %s "%s"', $filter->propertyName, $filter->operator, $filter->value));
									}
								}

								$linkEntityIDsRight = $query->fetchAll();
							} catch (\Dibi\Exception $ex) {
								throw new Exception($ex->getMessage() . ' | ' . $ex->getSql() . ' | ', 500, $ex->getPrevious());
							}

							if ($linkEntityIDsRight) {
								$childEntityIDs = [];
								$childEntityIDsLinked = [];
								foreach ($linkEntityIDsRight as $entityID) {
									if (!isset($childEntityIDsLinked[$entityID[$property->linkTableLeftColumn]])) {
										$childEntityIDsLinked[$entityID[$property->linkTableLeftColumn]] = [];
									}

									if (!in_array($entityID[$property->linkTableRightColumn], $childEntityIDsLinked[$entityID[$property->linkTableLeftColumn]])) {
										$childEntityIDsLinked[$entityID[$property->linkTableLeftColumn]][] = $entityID[$property->linkTableRightColumn];
									}

									if (!in_array($entityID[$property->linkTableRightColumn], $childEntityIDs)) {
										$childEntityIDs[] = $entityID[$property->linkTableRightColumn];
									}
								}

								$childLinkingProperty = $childEntityRepository->getPrimaryProperty();

								$childEntityFilters[] = new Filter($childLinkingProperty->name, $childEntityIDs, ORM_OPERATOR::IN);
								$childEntities = $this->getEntities($childEntityRepository, $childEntityFilters, null, [], $options, null, null, $maxDepth, $depth + 1);
							}
						}
					}

					if (empty($childEntities)) {
						if (!$property->isNullable) {
							$entities = [];
						}

						continue;
					}

					$childLinkingPropertyName = $childLinkingProperty->name;

					// pick related child entities and link them to a parent entity
					foreach ($entities as $entityIndex => $entity) {
						$childFound = false;

						if ($property->linkage === EntityProperty::LINKAGE_1_1) {
							foreach ($childEntities as $childEntity) {
								if ($entity->$propertyName === $childEntity->$childLinkingPropertyName) {
									$entity->$propertyName = $childEntity;

									$childFound = true;

									break;
								}
							}
						} elseif ($property->linkage === EntityProperty::LINKAGE_1_N) {
							$entityPrimaryValue = $repository->getEntityPrimaryValue($entity);

							foreach ($childEntities as $childEntity) {
								if ($childEntity->$childLinkingPropertyName === $entityPrimaryValue) {
									$entity->$propertyName[] = $childEntity;

									$childFound = true;
								}
							}
						} elseif ($property->linkage === EntityProperty::LINKAGE_N_N) {
							$entityPrimaryValue = $repository->getEntityPrimaryValue($entity);

							foreach ($childEntities as $childEntity) {
								if (!isset($childEntityIDsLinked[$entityPrimaryValue])) {
									continue;
								}

								$currChildEntityIDs = $childEntityIDsLinked[$entityPrimaryValue];
								if (in_array($childEntity->$childLinkingPropertyName, $currChildEntityIDs)) {
									$entity->$propertyName[] = $childEntity;

									$childFound = true;
								}
							}
						}

						if (!$property->isNullable && !$childFound) {
							unset($entities[$entityIndex]);
							continue;
						}
					}
				}
			}
		}

		return array_values($entities);
	}

	/**
	 * @param Filter $filter
	 *
	 * @return FilterGroup|null
	 */
	private function getFilterParentGroup(Filter $filter)
	{
		if (empty($this->withFilterGroups)) {
			return null;
		}

		foreach ($this->withFilterGroups as $group) {
			if (in_array($filter->hash, $group->filtersHashes)) {
				return $group;
			}
		}

		return null;
	}

	/**
	 * @param Filter[] $allFilters
	 * @param int $treeDepth
	 *
	 * @return Filter[]
	 */
	private function getApplicableFilters(array $allFilters, int $treeDepth)
	{
		$filters = [];
		foreach ($allFilters as $filter) {
			$parts = explode('.', $filter->propertyName);
			if (count($parts) === 1 || count($parts) === $treeDepth + 1) {
				$newFilter = new Filter($parts[count($parts) - 1], $filter->value, $filter->operator);
				$newFilter->hash = $filter->hash;
				$filters[] = $newFilter;
			}
		}

		return $filters;
	}

	/**
	 * @param $propertyName
	 * @param Filter[] $allFilters
	 * @param int $treeDepth
	 *
	 * @return Filter[]
	 */
	private function getFiltersForChildEntity($propertyName, array $allFilters, int $treeDepth)
	{
		$filters = [];
		foreach ($allFilters as $filter) {
			$parts = explode('.', $filter->propertyName);
			if (count($parts) < 2 || $treeDepth >= count($parts)) {
				continue;
			}

			if ($parts[$treeDepth] === $propertyName) {
				$filters[] = $filter;
			}
		}

		return $filters;
	}

	/**
	 * @param Repository $repository
	 * @param Row $row
	 *
	 * @return mixed
	 * @throws Exception
	 */
	protected function createEntity(Repository $repository, $row)
	{
		$entityClassName = 'ORM\\' . $repository->getEntityName() . 'Entity';
		if (!class_exists($entityClassName)) {
			throw new Exception(sprintf('Entity class "%s" not found', $entityClassName));
		}

		$entity = new $entityClassName();

		foreach ($repository->getProperties() as $property) {
			if (($this->isPropertyTypeBasic($property) || $property->linkage === EntityProperty::LINKAGE_1_1) &&
				property_exists($repository->getEntityClassName(), $property->name) && !$property->dontSelect) {
				$propertyName = $property->name;
				$entity->$propertyName = $row[$property->dbColumn];
			}
		}

		return $entity;
	}

	/**
	 * @param Fluent $query
	 * @param Repository $repository
	 *
	 * @return Fluent
	 */
	private function composeSelect(Fluent $query, Repository $repository)
	{
		foreach ($repository->getProperties() as $property) {
			if (($this->isPropertyTypeBasic($property) || $property->linkage === EntityProperty::LINKAGE_1_1) && !$property->dontSelect) {
				$query->select(sprintf('%s', $property->dbColumn));
			}
		}

		return $query;
	}

	/**
	 * @return bool
	 * @throws BadRequestException
	 * @throws Exception
	 */
	protected function save()
	{
		if (!$this->dbConnection) {
			throw new Exception('ORM is not connected to the database');
		}

		$this->saveResult = self::OK;
		$this->lastInsertID = 0;

		if (empty($this->withEntity) && empty($this->withEntities)) {
			throw new Exception(sprintf('No entity of type "%s" provided to save', $this->entityName));
		}

		if (!empty($this->withEntities)) {
			throw new Exception('Multiinsert and multiupdate not supported yet');
		} elseif (!empty($this->withEntity)) {
			$entity = clone($this->withEntity);
			$this->verifyEntityClass($entity);

			$primaryValue = $this->getEntityPrimaryValue($entity);

			if ($this->withOptions & ORM_OPTION::FORCE_REQUIRED_VALIDATION) {
				$this->validateRequiredValues((array)$entity);
			}

			if ($this->withOptions & ORM_OPTION::FORCE_TYPE_VALIDATION) {
				$this->validateDataTypes((array)$entity);
			}

			if ($this->withOptions & ORM_OPTION::FORCE_LIMIT_VALIDATION) {
				$this->validateLimitedValues((array)$entity);
			}

			try {
				if (empty($primaryValue)) {// insert
					$entity = $this->prepareEntityForSave($entity);
					if (empty((array)$entity)) {
						$this->saveResult = self::NOTHING_TO_DO;
						return true;
					}

					$query = $this->dbConnection->insert(sprintf('%s.%s', $this->getDatabaseName(), $this->getDatabaseTableName()), (array)$entity);

					if ($this->enabledDebug) {
						$this->latestSQLQueries[] = $this->getEntityName() . ' | ' . $query->__toString();
					}

					$query->execute();

					$this->saveResult = self::OK;
					$this->lastInsertID = $this->dbConnection->getInsertId();

					return true;
				} else {// update
					$entity = $this->prepareEntityForSave($entity, true);
					if (empty((array)$entity)) {
						$this->saveResult = self::NOTHING_TO_DO;
						return true;
					}

					$query = $this->dbConnection->update(sprintf('%s.%s', $this->getDatabaseName(), $this->getDatabaseTableName()), (array)$entity)
						->where(sprintf('%s = %s', $this->getPrimaryProperty()->dbColumn, $primaryValue));

					if ($this->enabledDebug) {
						$this->latestSQLQueries[] = $this->getEntityName() . ' | ' . $query->__toString();
					}

					$query->execute();

					if (!$this->dbConnection->getAffectedRows()) {
						$this->saveResult = self::NOTHING_TO_DO;
						return true;
					}

					$this->saveResult = self::OK;
					return true;
				}
			} catch (\Dibi\Exception | Exception $ex) {
				$this->saveResult = self::FAILURE;
				throw new Exception($ex->getMessage(), 500, $ex->getPrevious());
			}
		}
	}

	/**
	 * @return int
	 */
	public function getSaveResult()
	{
		return $this->saveResult;
	}

	/**
	 * @return int
	 */
	public function getLastInsertID()
	{
		return $this->lastInsertID;
	}

	/**
	 * @param mixed $entity
	 * @param bool $onlyChangedProperties
	 *
	 * @return mixed
	 * @throws Exception
	 */
	private function prepareEntityForSave($entity, bool $onlyChangedProperties = false)
	{
		$changedProperties = [];
		if ($onlyChangedProperties) {
			$changedProperties = $entity->getChangedProperties();
		}

		foreach ($this->getProperties() as $property) {
			$propertyName = $property->name;

			if ($onlyChangedProperties && !isset($changedProperties[$propertyName])) {
				unset($entity->$propertyName);
				continue;
			}

			if (!isset($entity->$propertyName) || is_array($entity->$propertyName) || is_object($entity->$propertyName)) {
				unset($entity->$propertyName);
				continue;
			}

			if ($property->linkage === EntityProperty::LINKAGE_1_1) {
				if (!empty($entity->$propertyName) && !Utils::isInt($entity->$propertyName)) {
					$childRepository = Manager::getInstance()->getRepository($this->getBarePropertyTypeName($property));
					$childPrimaryValue = $childRepository->getEntityPrimaryValue($entity->$propertyName);
					$entity->$propertyName = $childPrimaryValue;
				}
			} elseif ($property->linkage === EntityProperty::LINKAGE_1_N || $property->linkage === EntityProperty::LINKAGE_N_N) {
				unset($entity->$propertyName);
				continue;
			}
			if ($property->dbColumn !== $propertyName) {
			    $dbColumnName = $property->dbColumn;
			    
			    $entity->$dbColumnName = $entity->$propertyName;
			    unset($entity->$propertyName);
			}
		}

		$entity->removeHelperProperties();

		return $entity;
	}

	/**
	 * @param EntityProperty $property
	 *
	 * @return false|string
	 */
	private function getBarePropertyTypeName(EntityProperty $property)
	{
		$typeName = $property->type;
		$entityStringPos = strrpos($typeName, 'Entity');
		if ($entityStringPos === false || $entityStringPos + 6 < strlen($typeName)) {
			return $property->type;
		}

		$bareName = substr($typeName, 0, $entityStringPos);
		if ($bareName === false) {
			return $typeName;
		}

		return $bareName;
	}

	/**
	 * @param EntityProperty $property
	 *
	 * @return bool
	 */
	private function isPropertyTypeBasic(EntityProperty $property)
	{
		return $property->type === ORM_TYPE::Integer ||
			$property->type === ORM_TYPE::Number ||
			$property->type === ORM_TYPE::Bool ||
			$property->type === ORM_TYPE::String ||
			$property->type === ORM_TYPE::DateTime;
	}

	/**
	 * @param mixed $entity
	 *
	 * @throws Exception
	 */
	public function verifyEntityClass($entity)
	{
		if (get_class($entity) !== $this->entityClassName) {
			throw new Exception(sprintf('An entity of class "%s" expected. An instance of class "%s" given', $this->entityClassName, get_class($entity)));
		}
	}

	/**
	 * @param mixed$entity
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function getEntityPrimaryValue($entity)
	{
		$primaryPropertyName = $this->getPrimaryProperty()->name;

		if (!property_exists(get_class($entity), $primaryPropertyName)) {
			throw new Exception(sprintf('Property "%s" does nto exist in class "%s"', $primaryPropertyName, $this->entityClassName));
		}

		return $entity->$primaryPropertyName;
	}
}